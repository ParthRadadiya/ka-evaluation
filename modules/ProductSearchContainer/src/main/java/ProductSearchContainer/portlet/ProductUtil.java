package ProductSearchContainer.portlet;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.PortalUtil;

public class ProductUtil {
	
	public static void hideDefaultErrorMessage(PortletRequest portletRequest) {
		SessionMessages.add(portletRequest,
				PortalUtil.getPortletId(portletRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
	}

	public static void hideDefaultSucessMessage(PortletRequest portletRequest) {
		SessionMessages.add(portletRequest,
				PortalUtil.getPortletId(portletRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_SUCCESS_MESSAGE);
	}

}
