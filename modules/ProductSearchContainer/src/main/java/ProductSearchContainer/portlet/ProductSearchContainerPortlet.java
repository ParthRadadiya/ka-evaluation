package ProductSearchContainer.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;

import ProductBuilder.model.Product;
import ProductBuilder.service.ProductLocalService;
import ProductSearchContainer.constants.ProductSearchContainerPortletKeys;

/**
 * @author parth.radadiya
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ProductSearchContainerPortletKeys.ProductSearchContainer,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)

public class ProductSearchContainerPortlet extends MVCPortlet {

	@Reference
	ProductLocalService productLocalService;
	@Reference
	CounterLocalService counterLocalService;

	private static final Log LOG = LogFactoryUtil.getLog(ProductSearchContainerPortlet.class);

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		List<Product> product = productLocalService.getProducts(-1, -1);
		renderRequest.setAttribute("product", product);
		super.render(renderRequest, renderResponse);
	}

	/*
	 * ...........................................addURLMethod...................
	 * ... ...................................
	 */

	public void add(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception{

		LOG.info("################# Inside addStudentInfo #########################");

			ProductUtil.hideDefaultSucessMessage(actionRequest);
			actionResponse.setRenderParameter("jspPage", "/addUpdate.jsp");	
	}

	/*
	 * ...........................................UpdateUrlMethod...................
	 * ... ...................................
	 */

	public void UpdateURL(ActionRequest req, ActionResponse res) throws Exception {
		String Id1 = ParamUtil.getString(req, "Id");
		try {
			Product cobj = productLocalService.getProduct(Integer.parseInt(Id1));
			req.setAttribute("cobj", cobj);
			LOG.info("ID :" + cobj);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (PortalException e) {

			e.printStackTrace();
		}

	}

	/*
	 * ...........................................addUpdateMethod...................
	 * ... ...................................
	 */

	public void addUpdate(ActionRequest request, ActionResponse response) throws Exception {

		LOG.info("Added Method Called");

		Product product = productLocalService.createProduct((int) counterLocalService.increment());
		int Id = ParamUtil.getInteger(request, "Id");
		product.setId(Id);
		product.setName(ParamUtil.getString(request, "name"));
		product.setDescription(ParamUtil.getString(request, "description"));
		product.setPrice(ParamUtil.getInteger(request, "price"));
		product.setQuantity(ParamUtil.getInteger(request, "quantity"));
		
		if (Id == 0) {
			productLocalService.addProduct(product);
			add(request, response);
		}else {
			productLocalService.updateProduct(product);
			UpdateURL(request, response);
		}
	}
	
	/*
	 * ...........................................deleteMethod...................
	 * ... ...................................
	 */


	public void delete(ActionRequest request, ActionResponse response) throws Exception {

		LOG.info("Delete Method called");

		int id 	= ParamUtil.getInteger(request, "Id");
		Product pdelete = productLocalService.deleteProduct(id);

		LOG.info("Deleted Record :" + pdelete);

	}

}