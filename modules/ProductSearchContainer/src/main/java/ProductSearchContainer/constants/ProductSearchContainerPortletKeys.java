package ProductSearchContainer.constants;

/**
 * @author parth.radadiya
 */
public class ProductSearchContainerPortletKeys {

	public static final String ProductSearchContainer = "productsearchcontainer";

}