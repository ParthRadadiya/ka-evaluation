<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="init.jsp" %>   
    
<portlet:actionURL name="addUpdate" var="actionaddupdateURL">
<portlet:param name="mvcPath" value="/view.jsp" />		
</portlet:actionURL>
<portlet:renderURL windowState="normal" var="backURL">
<portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<liferay-ui:header backURL="${backURL}" title="Back" />
<br>

<c:set var="cobj" value="<%=request.getAttribute("cobj") %>" />

<form action="${actionaddupdateURL}" method="post">
		<div class="row">
		<label> Name :</label> <input type="hidden"
			name="<portlet:namespace/>Id" value = "${cobj.getId()}" placeholder="Enter Your Product Name"
			class="form-control" required="required">
	</div>
	<br>
	

	<div class="row">
		<label> Name :</label> <input type="text"
			name="<portlet:namespace/>name" value = "${cobj.name}" placeholder="Enter Your Product Name"
			class="form-control" required="required">
	</div>
	<br>
	<div class="row">
		<label>Description :</label>
		<textarea name="<portlet:namespace/>description" value = "${cobj.description}"
			placeholder="Description" class="form-control">${cobj.description}</textarea>
	</div>
	<br>
	<div class="row">
		<label>Price :</label> <input type="number" value = "${cobj.price}"
			name="<portlet:namespace/>price" class="form-control"
			placeholder="Product Price" required="required">
	</div>
	<br>
	<div class="row">
		<label>Quantity :</label> <input type="number" value="${cobj.quantity }"
			name="<portlet:namespace/>quantity" class="form-control"
			placeholder="Enter the Product Quntity" required="required">
	</div>
	<br>
	<div class="container">
		<div class="row">
			<div class="text-left">
				<div class="form-group col-sm-6">
					<input type="submit" class="btn-success" name="submit"
						value="${cobj.Id eq null?'Add':'Update' }">
				</div>
			</div>
		</div>
	</div>
</form>