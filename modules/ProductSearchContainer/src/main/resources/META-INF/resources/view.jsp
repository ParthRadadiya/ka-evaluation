<%@ include file="/init.jsp" %>
<%@page import="ProductBuilder.service.ProductLocalServiceUtil"%>

<portlet:actionURL name="add" var="directionURL">
</portlet:actionURL>

<aui:button-row>
	<aui:button class="btn btn-success"
		onClick="${directionURL.toString()}" value="Add Product"></aui:button>
</aui:button-row>

<br>


<liferay-ui:search-container>
	<liferay-ui:search-form page=""/>
	<liferay-ui:search-container-results results="${requestScope.product}"  />
	<liferay-ui:search-container-row className="ProductBuilder.model.Product" modelVar="Product" keyProperty="Id">
		<liferay-ui:search-container-column-text name="Poduct Name" property="name" />
		<liferay-ui:search-container-column-text name="Description" property="description" />
	    <liferay-ui:search-container-column-text name="Price" property="price"/>
		<liferay-ui:search-container-column-text name="Quantity" property="quantity" />
		<liferay-ui:search-container-column-jsp path="/action.jsp" align="center" />
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator />
</liferay-ui:search-container>