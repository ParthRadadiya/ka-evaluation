<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="init.jsp" %>
<c:set value = "${reqestScope.SEARCH_CONTAINER_RESULT_ROW}" var = "product1"></c:set>    
<c:set value = "${product1.object}" var = "product"></c:set>


<portlet:actionURL name="delete" var="deleteproduct">
<portlet:param name="mvcPath" value="/addUpdate.jsp"></portlet:param>
	<portlet:param name="Id"
		value="${product.Id}" />
</portlet:actionURL>

<aui:button-row>
	<aui:button class="btn btn-success"
		onClick="${deleteproduct.toString()}" value="Delete Product"></aui:button>
</aui:button-row>

<portlet:actionURL name="UpdateURL" var="Updateproduct">
<portlet:param name="mvcPath" value="/addUpdate.jsp"></portlet:param>
	<portlet:param name="Id"
		value="${product.Id}" />
</portlet:actionURL>

<aui:button-row>
	<aui:button class="btn btn-success"
		onClick="${Updateproduct.toString()}" value="Update Product"></aui:button>
</aui:button-row>