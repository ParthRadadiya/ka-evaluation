package customloginstrutsaction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.struts.BaseStrutsAction;
import com.liferay.portal.kernel.struts.StrutsAction;


@Component(immediate = true, property = { "path=/portal/login" }, service = StrutsAction.class)

public class CustomloginstrutsactionActivator extends BaseStrutsAction {

	private static final Log _Log = LogFactoryUtil.getLog(CustomloginstrutsactionActivator.class);

	@Override
	public String execute(StrutsAction originalStrutsAction, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		_Log.info("Struts Execute Mehod Called");
		
		return super.execute(originalStrutsAction, request, response);

	}

}