package postLoginSession;

import org.osgi.service.component.annotations.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.events.LifecycleEvent;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PortalUtil;

/**
 * @author parth.radadiya
 */

@Component(immediate = true, property = { "key=login.events.post" }, service = LifecycleAction.class)

public class PostLoginSessionActivator implements LifecycleAction {

	private static final Log LOG = LogFactoryUtil.getLog(PostLoginSessionActivator.class);

	@Override
	public void processLifecycleEvent(LifecycleEvent lifecycleEvent) throws ActionException {

		HttpServletRequest httpServletRequest = lifecycleEvent.getRequest();
		HttpSession httpSession = httpServletRequest.getSession();

		String username;
		try {
			username = PortalUtil.getUser(httpServletRequest).getFullName();
			httpSession.setAttribute("username", username);
			LOG.info("before session : " + httpSession.getAttribute("username"));
			httpServletRequest.getServletContext().setAttribute("usersession", httpSession);
			LOG.info("after session : " + httpSession.getAttribute("username"));
		} catch (PortalException e) {
			e.printStackTrace();

		}
	}
}