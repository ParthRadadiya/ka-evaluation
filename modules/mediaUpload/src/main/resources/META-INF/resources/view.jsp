<%@ include file="init.jsp" %>

<portlet:actionURL name="uploadFile" var="actionUrl"></portlet:actionURL>

<aui:form action="${actionUrl}" method="post" enctype="multipart/form-data">
	<aui-row>
		<aui:input name="upload" type="file" value=""/>
	</aui-row>
	<aui:button-row>
		<aui:button name="name" type="submit" value="Upload" ></aui:button>
	</aui:button-row>
	
</aui:form>
<script>
	 function upload(){
		var file = document.getElementById('file').value;
		var ext = file.split('.').pop();
		if(ext == 'pdf'){
			document.write("true");
		}else{
			document.write("false");
		}
	}
</script>