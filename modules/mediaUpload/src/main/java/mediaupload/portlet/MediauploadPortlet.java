package mediaupload.portlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true", "javax.portlet.display-name=mediaUpload Portlet",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)

public class MediauploadPortlet extends MVCPortlet {

	private static final Log _log = LogFactoryUtil.getLog(MediauploadPortlet.class);

	public void uploadFile(ActionRequest reqest, ActionResponse response) throws Exception {

		_log.info("uploadFile method called");

		ThemeDisplay themeDisplay = (ThemeDisplay) reqest.getAttribute(WebKeys.THEME_DISPLAY);
		_log.info(themeDisplay);
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(reqest);

		File file = uploadRequest.getFile("upload");

		InputStream is = new FileInputStream(file);

		String filename = uploadRequest.getFileName("upload");

		ServiceContext serviceContext = null;
		
		DLFolder dlfolder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, "MediaCustomUpload");
		
		serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(), uploadRequest);

		try {
			// DLFileEntry dlfileentry =
			// DLFileEntryLocalServiceUtil.addFileEntry(themeDisplay.getUserId()
			//
			// ,themeDisplay.getScopeGroupId(), themeDisplay.getScopeGroupId(),
			// 0L,uploadRequest.getContentType("upload"), filename, filename,
			// filename, filename, 0L, null, file, is, file.length(),
			// serviceContext);
			
			DLAppLocalServiceUtil.addFileEntry(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(), dlfolder.getFolderId(), filename,
					uploadRequest.getContentType("upload"), filename, filename, "", is, file.length(), serviceContext);

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

}