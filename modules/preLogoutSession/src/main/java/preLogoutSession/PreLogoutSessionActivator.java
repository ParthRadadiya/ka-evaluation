package preLogoutSession;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.events.LifecycleEvent;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PortalUtil;

/**
 * @author parth.radadiya
 */
@Component(immediate = true, property = { "key=logout.events.pre" }, service = LifecycleAction.class)

public class PreLogoutSessionActivator implements LifecycleAction {

	private static final Log LOG = LogFactoryUtil.getLog(PreLogoutSessionActivator.class);

	@Override
	public void processLifecycleEvent(LifecycleEvent lifecycleEvent) throws ActionException {

		HttpServletRequest httpServletRequest = lifecycleEvent.getRequest();

		HttpSession httpSession = httpServletRequest.getSession();

		/* httpSession.invalidate(); */

		/* LOG.info("Your Session close"); */

		String username;

		try {
			username = PortalUtil.getUser(httpServletRequest).getFullName();
			httpSession.setAttribute("username", username);
			/*LOG.info("before session : " + httpSession.getAttribute("username"));*/
			httpServletRequest.getServletContext().setAttribute("usersession", httpSession);
			LOG.info("Session is ON : UserName = " + httpSession.getAttribute("username"));
		} catch (Exception e) {
			e.printStackTrace();
			httpServletRequest.setAttribute("usersession", null);
		}
		
		if (httpSession != null) {
			
			httpSession.removeAttribute("username");
			LOG.info("username session is out : Username = " + httpSession.getAttribute("username"));
			LOG.info("username session is out : LoginSession = " + httpSession.getAttribute("usersession"));
			httpSession.invalidate();
			LOG.getClass();
			LOG.info("Session is closed");
		}else {
			LOG.info("Session is Failed closed");
		}
	}
}