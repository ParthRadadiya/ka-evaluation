create table FOO_Product (
	id_ INTEGER not null primary key,
	name VARCHAR(75) null,
	description VARCHAR(75) null,
	price INTEGER,
	quantity INTEGER
);