<%@ include file="/init.jsp"%>

<portlet:renderURL var="directionURL">
	<portlet:param name="mvcPath" value="/addProduct.jsp"></portlet:param>
</portlet:renderURL>

<aui:button-row>
	<aui:button class="btn btn-success"
		onClick="${directionURL.toString()}" value="Add Entry"></aui:button>
</aui:button-row>

<br>
<portlet:renderURL var="ViewURL">
	<portlet:param name="mvcPath" value="/viewProduct.jsp"></portlet:param>
</portlet:renderURL>

<aui:button-row>
	<aui:button class="btn btn-danger" onClick="${ViewURL.toString()}"
		value="View Entry"></aui:button>
</aui:button-row>
