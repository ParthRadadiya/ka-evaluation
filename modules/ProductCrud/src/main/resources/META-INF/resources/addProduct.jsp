<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ include file="init.jsp" %>
<portlet:actionURL name="add" var="actionaddURL">
	<portlet:param name="mvcPath" value="/view.jsp" />
</portlet:actionURL>

<form action="${actionaddURL}" method="post">
	<div class="row">
		<label> Name :</label> <input type="text"
			name="<portlet:namespace/>name" placeholder="Enter Your Product Name"
			class="form-control" required="required">
	</div>
	<br>
	<div class="row">
		<label>Description :</label>
		<textarea name="<portlet:namespace/>description"
			placeholder="Description" class="form-control"></textarea>
	</div>
	<br>
	<div class="row">
		<label>Price :</label> <input type="number"
			name="<portlet:namespace/>price" class="form-control"
			placeholder="Product Price" required="required">
	</div>
	<br>
	<div class="row">
		<label>Quantity :</label> <input type="number"
			name="<portlet:namespace/>quantity" class="form-control"
			placeholder="Enter the Product Quntity" required="required">
	</div>
	<br>
	<div class="container">
		<div class="row">
			<div class="text-left">
				<div class="form-group col-sm-6">
					<input type="submit" class="btn-success" name="submit"
						value="Submit">
				</div>
			</div>
		</div>
	</div>
</form>
