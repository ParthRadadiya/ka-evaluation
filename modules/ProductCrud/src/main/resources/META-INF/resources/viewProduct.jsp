<%@page import="ProductBuilder.service.ProductLocalServiceUtil"%>
<%@page import="ProductBuilder.model.Product"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="init.jsp"%>
<%
		int count = ProductLocalServiceUtil.getProductsCount();
		List<Product> views = ProductLocalServiceUtil.getProducts(0, count);
	%>

<div class="table-responsive">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th id="th1">Product Name</th>
				<th id="th2">Description</th>
				<th id="th3">Price</th>
				<th id="th4">Quantity</th>
			</tr>
			<%
					for (Product view : views) {
				%>
		</thead>
		<tbody>
			<tr>
				<td><%=view.getName()%></td>
				<td><%=view.getDescription()%></td>
				<td><%=view.getPrice()%></td>
				<td><%=view.getQuantity()%></td>
				<td><portlet:actionURL name="UpdateURL" var="UpdateURL">
						<portlet:param name="mvcPath" value="/updateProduct.jsp"></portlet:param>
						<portlet:param name="Id" value="<%=String.valueOf(view.getId())%>" />
					</portlet:actionURL> <aui:button-row>
						<aui:button class="btn btn-success"
							onClick="${UpdateURL.toString()}" value="Update"></aui:button>
					</aui:button-row> <portlet:actionURL name="delete" var="actiondeleteURL">
						<portlet:param name="mvcPath" value="/viewProduct.jsp"></portlet:param>
						<portlet:param name="Id" value="<%=String.valueOf(view.getId())%>" />
					</portlet:actionURL> <a href="${actiondeleteURL.toString()}">Delete</a></td>

			</tr>
			<%
					}
				%>
		</tbody>
	</table>
</div>

</div>