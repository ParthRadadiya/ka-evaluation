package ProductCrud.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;

import ProductBuilder.model.Product;
import ProductBuilder.service.ProductLocalService;
import ProductCrud.constants.ProductCrudPortletKeys;

/**
 * @author parth.radadiya
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp", "javax.portlet.name=" + ProductCrudPortletKeys.ProductCrud,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)

public class ProductCrudPortlet extends MVCPortlet {
	@Reference
	ProductLocalService productLocalService;
	@Reference
	CounterLocalService counterLocalService;

	private static final Log LOG = LogFactoryUtil.getLog(ProductCrudPortlet.class);

	public void add(ActionRequest request, ActionResponse response) throws Exception {

		LOG.info("Added Method Called");

		Product product = productLocalService.createProduct((int) counterLocalService.increment());

		product.setName(ParamUtil.getString(request, "name"));
		product.setDescription(ParamUtil.getString(request, "description"));
		product.setPrice(ParamUtil.getInteger(request, "price"));
		product.setQuantity(ParamUtil.getInteger(request, "quantity"));

		productLocalService.addProduct(product);
	}

	/*
	 * ...........................................UpdateUrlMethod...................
	 * ... ...................................
	 */

	public void UpdateURL(ActionRequest req, ActionResponse res) throws Exception {
		String Id1 = ParamUtil.getString(req, "Id");
		try {
			Product cobj = productLocalService.getProduct(Integer.parseInt(Id1));
			req.setAttribute("cobj", cobj);
			LOG.info("ID :" + cobj);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (PortalException e) {

			e.printStackTrace();
		}

	}

	/*
	 * ...........................................UpdateMethod......................
	 * ...................................
	 */
	public void UpateData(ActionRequest request, ActionResponse response) throws Exception {

		LOG.info("Added Method Called");
		String id = request.getParameter("Id");
		LOG.info("Id Found" + id);
		Product productUpdate = productLocalService.fetchProduct(Integer.parseInt(id));

		productUpdate.setName(ParamUtil.getString(request, "name"));
		productUpdate.setDescription(ParamUtil.getString(request, "description"));
		productUpdate.setPrice(ParamUtil.getInteger(request, "price"));
		productUpdate.setQuantity(ParamUtil.getInteger(request, "quantity"));

		productLocalService.updateProduct(productUpdate);
	}

	/*
	 * ...........................................DeleteMethod......................
	 * ...................................
	 */

	public void delete(ActionRequest request, ActionResponse response) throws Exception {

		LOG.info("Delete Method called");

		int id = ParamUtil.getInteger(request, "Id");
		Product pdelete = productLocalService.deleteProduct(id);

		LOG.info("Deleted Record :" + pdelete);

	}
}